--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Debian 13.3-1.pgdg100+1)
-- Dumped by pg_dump version 13.3 (Debian 13.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: stardata; Type: TABLE; Schema: public; Owner: reddening
--

CREATE TABLE public.stardata (
                                 identification character varying NOT NULL,
                                 x double precision NOT NULL,
                                 y double precision NOT NULL,
                                 distance_from_the_sun double precision NOT NULL,
                                 reddening double precision NOT NULL
);


ALTER TABLE public.stardata OWNER TO reddening;

--
-- Data for Name: stardata; Type: TABLE DATA; Schema: public; Owner: reddening
--

COPY public.stardata (identification, x, y, distance_from_the_sun, reddening) FROM stdin;
astar	5	5.4	5.39	39
bstar	100	100.4	100.39	390
cstar	0	100.4	100.39	10
dstar	100	0	100.39	110
estar	100	100.54	0.39	1
fstar	100	0.54	0.39	391
gstar	0	100.54	0.39	9
hstar	0	0.54	100.39	99
istar	0	0.54	0.39	0
\.


--
-- Name: stardata stardata_pkey; Type: CONSTRAINT; Schema: public; Owner: reddening
--

ALTER TABLE ONLY public.stardata
    ADD CONSTRAINT stardata_pkey PRIMARY KEY (identification);


--
-- PostgreSQL database dump complete
--
