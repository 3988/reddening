from __future__ import print_function

import os
os.environ["DUST_DIR"] = "/root/data/"

import mwdust
import numpy
import sys
from astropy.coordinates import SkyCoord
import astropy.units as units


mapID = sys.argv[1]
frameID = sys.argv[2]

x = float(sys.argv[3])
y = float(sys.argv[4])
dist = float(sys.argv[5])

if mapID == "Drimmel":
	drimmel= mwdust.Drimmel03(filter='2MASS H')
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	if frameID == "icrs":
		coords = coords.galactic
	print(drimmel(coords.l.value,coords.b.value,coords.distance.value)) 
elif mapID == "Green17":
	green17= mwdust.Green17(filter='2MASS H')
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)	
	if frameID == "icrs" :
		coords = coords.galactic
	print(green17(coords.l.value,coords.b.value,coords.distance.value)) 
elif mapID == "Combined15":
	combined15= mwdust.Combined15(filter='2MASS H')
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	if frameID == "icrs":
		coords = coords.galactic
	print(combined15(coords.l.value,coords.b.value,coords.distance.value)) 
elif mapID == "Schlegel":
	from dustmaps.sfd import SFDQuery
	sfd= SFDQuery()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(sfd(coords))
elif mapID== "Green15":
	green15= mwdust.Green15(filter='2MASS H')
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	if frameID == "icrs":
		coords = coords.galactic
	print(green15(coords.l.value,coords.b.value,coords.distance.value)) 
elif mapID=="Planck":
	from dustmaps.planck import PlanckQuery
	planck = PlanckQuery()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(planck(coords)) 
elif mapID=="BH":
	from dustmaps.bh import BHQuery
	bh = BHQuery()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(bh(coords)) 
elif mapID=="iphas":
	from dustmaps.iphas import IPHASQuery
	iphas = IPHASQuery()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(iphas(coords))
elif mapID=="le2019":
	from dustmaps.leike_ensslin_2019 import LeikeEnsslin2019Query
	le2019 = LeikeEnsslin2019Query()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(le2019(coords))
elif mapID=="lenz2017":
	from dustmaps.lenz2017 import Lenz2017Query
	lenz2017 = Lenz2017Query()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(lenz2017(coords))
elif mapID=="Marshall":
	from dustmaps.marshall import MarshallQuery
	marshall = MarshallQuery()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(marshall(coords,return_sigma=True))
elif mapID=="pg2010":
	from dustmaps.pg2010 import PG2010Query
	pg2010 = PG2010Query()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(pg2010(coords))
elif mapID=="PlanckGNILC":
	from dustmaps.planck import PlanckGNILCQuery
	planck = PlanckGNILCQuery(load_errors= True)
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(planck(coords))
elif mapID=="Chen2014":
	from dustmaps.chen2014 import Chen2014Query
	chen = Chen2014Query()
	coords = SkyCoord(x*units.deg,y*units.deg,dist*units.pc, frame= frameID)
	print(chen(coords,return_sigma= True))
else:
	print("map not implemented yet")

	
