This repository contains code of Bachelor Thesis of Martin Striežovský,
Student of Applied Informatics at Masaryk's University in Brno,
Web Application for Estimation of Reddening of Stars.

Main code contains main code which needs compiling,
Python scripts contain scripts called by application to output values.
Maps present in official application are not included as they are several
gigabytes large.

Acknowledgments:
I would first of all like to thank my advisor, RNDr. Martin Kuba,
Ph.D and my consultant Ernst Paunzen, Dr.rer.nat for 
endless support they provided me with. I am thankful to Dr. 
Gregory M. Green for providing me with lost map files. I 
would also like to thank my parents and everyone who 
supported me in my studies
