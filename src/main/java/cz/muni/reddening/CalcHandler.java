package cz.muni.reddening;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class which handles individual reddening calculations by calling Python script
 */
@Component
public class CalcHandler {

    private static final Logger log = LoggerFactory.getLogger(CalcHandler.class);

    private final Configuration configuration;

    public CalcHandler(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Runs Python script to calculate Reddening
     * @return last line of output of python script, meaning the correct calculated reddening or error message
     */
    String runScript(String map, String frame, String x, String y, String dist) {
        log.info("run {} {} {} {} {} {} {}", configuration.getPython(), configuration.getScriptPath(), map, frame, x, y, dist);
        ProcessBuilder scriptBuilder = new ProcessBuilder(configuration.getPython(), configuration.getScriptPath(), map, frame, x, y, dist);
        scriptBuilder.redirectErrorStream(true);
        List<String> results;
        Process script;
        try {
            script = scriptBuilder.start();
            script.waitFor();
            results = new BufferedReader(new InputStreamReader(
                    script.getInputStream()))
                    .lines().collect(Collectors.toList());
        } catch (IOException | InterruptedException e) {
            log.error("python script failed", e);
            return "there has been error calculating data";
        }
        log.info("success {} {} {} {} {} {}", map, frame, x, y, dist, results.get(results.size() - 1));
        return results.get(results.size() - 1);
    }
}
