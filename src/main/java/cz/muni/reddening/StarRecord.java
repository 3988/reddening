package cz.muni.reddening;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("SpellCheckingInspection")
final class StarRecord {
    private final String identification;
    private final String x;
    private final String y;
    private final String distance_from_the_sun;
    private final String reddening;
    private final String eucldist;
    private static final Logger log = LoggerFactory.getLogger(StarRecord.class);

    public String getIdentification() {
        return identification;
    }

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }

    @SuppressWarnings("unused")
    public String getDistance_from_the_sun() {
        return distance_from_the_sun;
    }

    public String getReddening() {
        return reddening;
    }

    @SuppressWarnings("unused")
    public String getEucldist() {
        return eucldist;
    }

    /**
     * Constructor of StarRecord class
     * @param identification is the identification of the Star
     * @param x is first coordinate of Star location in galactic system (degrees)
     * @param y is the second coordinate of Star location in galactic system (degrees)
     * @param distance is the third coordinate of Star in galactic system (parsecs)
     * @param reddening is the reddening value of the Star
     * @param eucldist is the distance from queried point
     */
    StarRecord(final String identification, final String x, final String y,
               final String distance, final String reddening, final String eucldist) {
        this.identification = identification;
        this.x = x;
        this.y = y;
        this.distance_from_the_sun = distance;
        this.reddening = reddening;
        this.eucldist = eucldist;
    }

    /**
     * function converting StarRecord to JSON
     * @return StarRecord in JSON format
     */
    String toJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            log.info("json processing exception", e);
        }
        return "";
    }
}


