package cz.muni.reddening;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Class which handles querying database
 */
@SuppressWarnings({"SqlNoDataSourceInspection", "SqlResolve"})
@Component
public final class SQLHandler {
    private static final Logger log = LoggerFactory.getLogger(SQLHandler.class);

    private JdbcTemplate jdbc;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    /**
     * @return result of query as object to be formatted to JSON format
     */
    public Map<String,Object> execute(float x, float y, float dist, String sort,
                          String order, int offset, int limit) {
        log.info("SQLHandler execute: {} {} {} {} {} {} {}", x, y, dist, sort, order, offset, limit);
        TimeZone timeZone = TimeZone.getTimeZone("UTC"); // e.g. "Europe/Rome"
        TimeZone.setDefault(timeZone);
        String sql = "SELECT identification,"
                + " X, Y, Distance_from_the_Sun, Reddening, "
                + "(" + (Math.pow(dist, 2))
                + "+ POWER(Distance_from_the_Sun, 2) - "
                + "2 * Distance_from_the_Sun *"
                + dist
                + " * COS (" + (x / 180 * Math.PI) + ")"
                + " * COS (X/180*PI()) * COS(Y/ 180 *PI() - "
                + (y / 180 * Math.PI) + ") -" + "2 * Distance_from_the_Sun *" + dist
                + " * SIN (" + (x / 180 * Math.PI) + ") "
                + "* SIN (X/180*PI())"
                + ")" + " as eucldist FROM stardata"
                + " ORDER BY " + sort + " " + order + " LIMIT " + limit + " OFFSET " + offset;
        List<StarRecord> stars = jdbc.query(sql, (rs, i) ->
                new StarRecord(
                        rs.getString("identification"),
                        rs.getString("X"),
                        rs.getString("Y"),
                        rs.getString("Distance_from_the_Sun"),
                        rs.getString("Reddening"),
                        rs.getString("eucldist")
                )
        );
        Long count = jdbc.queryForObject("select count(*) from stardata", Long.class);
        log.info("success querying database {}", count);
        Map<String,Object> result =  new LinkedHashMap<>();
        result.put("total", count);
        result.put("rows", stars);
        return result;
    }
}
