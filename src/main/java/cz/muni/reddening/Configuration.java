package cz.muni.reddening;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Class which holds configuration of the application.
 */
@Component
@ConfigurationProperties(prefix = "configuration")
public final class Configuration {

    private String scriptPath = "/home/reddening/scripts/reddeningCalc.py";
    private String python = "/usr/bin/python3";

    public String getScriptPath() {
        return scriptPath;
    }

    public void setScriptPath(String scriptPath) {
        this.scriptPath = scriptPath;
    }

    public String getPython() {
        return python;
    }

    public void setPython(String python) {
        this.python = python;
    }
}
