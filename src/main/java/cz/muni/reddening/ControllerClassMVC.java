package cz.muni.reddening;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Spring Controller which catches requests on URLs and responds to them
 */
@Controller
public class ControllerClassMVC {
    private static final Logger log = LoggerFactory.getLogger(ControllerClassMVC.class);

    private final SQLHandler sqlHandler;
    private final CalcHandler calcHandler;
    private final FileHandler fileHandler;

    public ControllerClassMVC(SQLHandler sqlHandler, CalcHandler calcHandler, FileHandler fileHandler) {
        this.sqlHandler = sqlHandler;
        this.calcHandler = calcHandler;
        this.fileHandler = fileHandler;
    }

    /**
     * Estimation by values
     *
     * @param x        is the first coordinate in degrees
     * @param y        is the second coordinate in degrees
     * @param distance is the third coordinate in parsecs
     * @param law      is the reddening law/map we use
     * @param frame    is the coordinate system we use
     * @return correct reddening output or error message of the script
     */
    @RequestMapping(value = "/check", method = RequestMethod.POST)
    @ResponseBody
    public String check(@RequestParam final float x, @RequestParam final float y,
                        @RequestParam final float distance, @RequestParam final String law,
                        @RequestParam final String frame) {
        return calcHandler.runScript(law, frame, String.valueOf(x), String.valueOf(y), String.valueOf(distance));
    }

    /**
     * Estimation from file input
     *
     * @param law   is the reddening law/map we use
     * @param file  is the file we got from user
     * @param frame is coordinate frame used for calculations
     * @return the content of output file
     * @throws IOException if unable to process the input file
     */
    @RequestMapping(value = "/checkF", method = RequestMethod.POST)
    @ResponseBody
    public String checkFile(@RequestParam String law,
                            @RequestParam MultipartFile file,
                            @RequestParam String frame) throws IOException {
        return fileHandler.output(file, law, frame);

    }

    /**
     * Queries the database to show user individual reddening values of stars
     *
     * @param x      is the first coordinate in degrees
     * @param y      is the second coordinate in degrees
     * @param dist   is the third coordinate in parsecs
     * @param order  specifies if we sort records ascending or descending
     * @param sort   is the attribute by which we sort
     * @param limit  is the number from which we show queries
     * @param offset is the number from which we dont show queries
     * @return output of the query we make in JSON format
     */
    @SuppressWarnings("SpellCheckingInspection")
    @RequestMapping(value = "/query", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object queryDatabase(@RequestParam final float x, @RequestParam final float y,
                                @RequestParam final float dist, @RequestParam final String order,
                                @RequestParam final String sort, @RequestParam final int limit,
                                @RequestParam final int offset) {
        if (!(order.equalsIgnoreCase("asc")
                || order.equalsIgnoreCase("desc"))) {
            log.error("invalid order");
            return "Invalid input data";
        }
        switch (sort.toLowerCase()) {
            case "eucldist":
            case "x":
            case "y":
            case "distance_from_the_sun":
            case "reddening":
            case "identification":
                break;
            default:
                log.error("SQL invalid sort attribute");
                return "Invalid input data";
        }
        return sqlHandler.execute(x, y, dist, sort, order, offset, limit);
    }

    /**
     * Loads base site of the application
     *
     * @return application page
     */
    @RequestMapping(value =  { "/", "/index.html" })
    public String baseLoad() {
        return "index";
    }

}
